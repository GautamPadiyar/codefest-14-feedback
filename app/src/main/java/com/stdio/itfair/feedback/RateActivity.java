package com.stdio.itfair.feedback;

import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;



public class RateActivity extends Activity{

    public final String NAME = "Name : ";
    public final String FEEDBACK = "Feedback : ";
    public Button send;
    public Button delete;
    public EditText name;
    public EditText feedback;
    public String completeFeedback;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate);
        send = (Button) findViewById(R.id.saveButton);
        delete = (Button) findViewById(R.id.deleteButton);
        name = (EditText) findViewById(R.id.NameText);
        feedback = (EditText) findViewById(R.id.FeedText);



         }


     @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.hello, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if(id == R.id.about) {
            Intent open = new Intent(this , AboutUs.class);
            startActivity(open);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void save(View v){
        String n =  name.getText().toString();
        String f = feedback.getText().toString();
        completeFeedback = NAME + n + " "+ FEEDBACK + f;

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{ "codefest.skch@gmail.com"});
        email.putExtra(Intent.EXTRA_SUBJECT, "Codefest 2014 feedback");
        email.putExtra(Intent.EXTRA_TEXT, completeFeedback);
        email.setType("message/rfc822");

        startActivity(Intent.createChooser(email, "Choose an Email client :"));
        }

    public void delete(View v){
        Intent i = new Intent(this , HelloActivity.class);
        startActivity(i);
    }


}
